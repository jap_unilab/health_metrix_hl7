$( document ).ready(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
});    

function readFile(){
  event.preventDefault();
  $("#myModal").modal("show");
    $("#mod-title").html("<b>Upload HL7 File:</b>");
    $("#editloatypediv").addClass("hidden");
    $("#addLoaDiv").removeClass("hidden");
    $("#mod-content").html(
      '<div class="col-md-12">'+
        '<div class="form-group type-div" >'+
              '<label for="exampleFormControlFile1">HL7 File</label>'+
              '<input type="file" class="form-control-file" id="exampleFormControlFile1">'+
        '</div>'+
      '</div>'
    );
}