@extends('layouts.layout')

@section('css')
@endsection


@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">HL7</h1>
  <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"  onclick="readFile()"><i class="fas fa-download fa-sm text-white-50"></i> Read HL7 File</a>
</div>
<!-- Content Row -->
<div class="row">



  <div class="col-lg-12 mb-4">

    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">HL7 Data</h6>
      </div>
      <div class="card-body">
        <p></p>
      </div>
    </div>

  </div>
</div>


</div>
<!-- /.container-fluid -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id='mod-title'></h4>
            </div>
            <div class="modal-body">
            <div id='mod-content'>
            </div>
            </div>
            <div class="modal-footer">
            <span class="pull-left" id="notif-mess"></span>
            <div id="mod-footer"></div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script type="text/javascript" src="{{ asset('assets/js/hm_hl7.js') }}"></script>
@endsection